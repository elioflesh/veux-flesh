# veux-flesh Prerequisites

- [elioWay prerequisites](/prerequisites.html)
- [elioangels prerequisites](/elioangels/prerequisites.html)

```
npm i -g @vue/cli
npm init vite-app
npm i --save vuex
npm i --save "file:../sanitize-vue3-plugin"
npm i --save-dev prettier
```

## `package.json`

```
"scripts": {
  ...
  "prettier": "prettier --write \"**/*.{css,html,js,json,md,mjs,scss,ts,yaml}\" \"!package.json\""
}
```
