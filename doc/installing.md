# Installing veux-flesh

## Prerequisites

- [elioWay Prerequisites](/installing.html)
- [elioflesh Prerequisites](/elioflesh/installing.html)

## npm

Install into your SASS projects.

```
npm install @elioway/veux-flesh
yarn add @elioway/veux-flesh
```
