# veux-flesh Credits

## Artwork

- [art](https://publicdomainvectors.org/en/free-clipart/Marriage-proposal/54207.html)

## Core Thanks!

- [elioWay](https://elioway.gitlab.io)
- [Yeoman](http://yeoman.io/)

## Helpful

- <https://www.raymondcamden.com/2019/11/26/sanitizing-html-in-vuejs>
- <https://codepen.io/lacedon/pen/zwYbJV>

## Read This!

- <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl>
