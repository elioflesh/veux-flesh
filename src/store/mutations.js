if (navigator.webdriver) {
  window.localStorage.clear()
}

export const mutationsThing = {
  updateMutater(state, { value, thing, field }) {
    thing[field] = value
  },
  filteredBy(state, { field, value }) {
    state.APP.list.filtered = { field, value }
  }
}
