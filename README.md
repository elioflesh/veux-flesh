![](https://elioway.gitlab.io/elioflesh/veux-flesh/elio-veux-flesh-logo.png)

> Veux in the flesh, **the elioWay**

# veux-flesh ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

Vues written around **eliothing** objects, **the elioWay**.

- [veux-flesh Documentation](https://elioway.gitlab.io/elioflesh/veux-flesh/)

## Installing

- [Installing veux-flesh](https://elioway.gitlab.io/elioflesh/veux-flesh/installing.html)

## Requirements

- [elioflesh Prerequisites](https://elioway.gitlab.io/elioflesh/prerequisites.html)

## Seeing is Believing

```
npm i
npm run dev
```

- [elioflesh Quickstart](https://elioway.gitlab.io/elioflesh/quickstart.html)
- [veux-flesh Quickstart](https://elioway.gitlab.io/elioflesh/veux-flesh/quickstart.html)

# Credits

- [veux-flesh Credits](https://elioway.gitlab.io/elioflesh/veux-flesh/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioflesh/veux-flesh/apple-touch-icon.png)
